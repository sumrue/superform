import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import dataV from '@jiaminghi/data-view'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import formCreate from '@form-create/element-ui'
import FcDesigner from '@form-create/designer'

Vue.config.productionTip = false
Vue.prototype.$axios = axios
Vue.use(dataV)
Vue.use(ElementUI)
Vue.use(formCreate)
Vue.use(FcDesigner)
new Vue({
  render: (h) => h(App),
}).$mount('#app')
